﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Kool
{
	class Program
	{
		static void Main(string[] args)
		{

		}

		class Person
		{
			string _Nimi;
			string _Isikukood;
			public static List<string> Koodid = new List<string>();

			string _IsikukoodiKontroll(string x)
			{
				bool olemas = false;
				foreach (var y in Koodid)
				{
					if (x == y)
					{
						olemas = true;
						Console.WriteLine("Sellise isikukoodiga inimene on juba olemas!");
						break;
					}
				}
				if (!olemas)
				{
					double esim = Char.GetNumericValue(x[0]); //Stringi kujul ID esimene number, et oleks kergem kontrollida õigsust

					//ID-koodi reeglite kontroll
					if (esim > 0 && esim < 7)
					{
						StringBuilder aeg = new StringBuilder(); //ID-koodist sünniaja välja lugemine
						for (var i = 1; i < 7; i++)
						{
							aeg.Append(Char.GetNumericValue(x[i]));
						}

						//Kontrollime, kas sisestatud kuupäev on olemas ning võimalusel lisame isikukoodi listi
						try
						{
							DateTime.ParseExact(aeg.ToString(), "yyMMdd", null);
							StringBuilder kood = new StringBuilder();
							for (var i = 0; i < x.Length; i++)
							{
								kood.Append(Char.GetNumericValue(x[i]));
							}
							_Isikukood = kood.ToString();
							Koodid.Add(kood.ToString());
						}
						catch
						{
							Console.WriteLine("Vale isikukood!");
						}
					}
					else { Console.WriteLine("Vale isikukood!"); }
				}
				return this.ToString();
			}

			public string Nimi
			{
				get { return _Nimi; }
			}
			public string Isikukood
			{
				get { return _Isikukood; }
			}

			//Uus konstruktor
			public Person(string nimi)
			{
				Random r = new Random();
				_Nimi = nimi;
				_IsikukoodiKontroll($"{r.Next(5, 7)}{DateTime.Now.ToString("yyMMddffff")}");
			}
			public Person(string nimi, string kood)
			{
				_Nimi = nimi;
				_IsikukoodiKontroll(kood);
			}

			public override string ToString()
			{
				return $"{Nimi}, {Isikukood}";
			}

		}

	}
}
